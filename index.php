<?php
//Endpoint: http://213.122.166.85/LTMAPITest/ECommerce_WebServ.asmx?op=SelectCourseDetails
//AppName: Vivid Test
//ConnectionID: VIVID000001

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://213.122.166.85/LTMAPITest/ECommerce_WebServ.asmx?op=SelectCourseDetails",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n  <soap12:Body>\r\n    <SelectCourseDetails xmlns=\"http://tempuri.org/ECommerce_WS/Methods\">\r\n      <psXMLParams>500</psXMLParams>\r\n    </SelectCourseDetails>\r\n  </soap12:Body>\r\n</soap12:Envelope>",
  CURLOPT_HTTPHEADER => array(
    "appname: Vivid Test",
    "cache-control: no-cache",
    "connectionid: VIVID000001",
    "content-type: text/xml",
    "postman-token: 40e33690-e36e-c0d5-ad6c-cb870b505a45"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
?>